<?php

namespace Niqab\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{
    public function showAction()
    {
        $categories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findAll();

        $sorted = array();
        foreach ($categories as $cat) {
            if ($cat->getLevel() == 1) {
                $sorted[$cat->getId()]['parent'] = $cat;
                foreach ($categories as $subCat) {
                    if ($cat->getId() == $subCat->getParentId()) {
                        $sorted[$cat->getId()]['subs'][] = $subCat;
                    }
                }
            }
        }

        $sortFunc = function ($a, $b) {
            if ($a['parent']->getMenuOrder() == $b['parent']->getMenuOrder()) {
                return 0;
            }
            return ($a['parent']->getMenuOrder() < $b['parent']->getMenuOrder()) ? -1 : 1;
        };

        uasort($sorted, $sortFunc);

        return $this->render('NiqabApplicationBundle:Menu:show.html.twig', array('menu' => $sorted));
    }

    public function footerAction()
    {
        $categories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findAll();

        $sorted = array();
        foreach ($categories as $cat) {
            if ($cat->getLevel() == 1) {
                $sorted[$cat->getId()]['parent'] = $cat;
                foreach ($categories as $subCat) {
                    if ($cat->getId() == $subCat->getParentId()) {
                        $sorted[$cat->getId()]['subs'][] = $subCat;
                    }
                }
            }
        }

        $sortFunc = function ($a, $b) {
            if ($a['parent']->getMenuOrder() == $b['parent']->getMenuOrder()) {
                return 0;
            }
            return ($a['parent']->getMenuOrder() < $b['parent']->getMenuOrder()) ? -1 : 1;
        };

        uasort($sorted, $sortFunc);

        return $this->render('NiqabApplicationBundle:Menu:footer.html.twig', array('menu' => $sorted));
    }
}