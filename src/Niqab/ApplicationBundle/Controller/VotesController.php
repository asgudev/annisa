<?php

namespace Niqab\ApplicationBundle\Controller;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class VotesController extends Controller
{
    public function showAllAction()
    {
        $votes = $this->getDoctrine()->getRepository('SCVotesBundle:Votes')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $votes,
            $this->get('request')->query->get('page', 1),
            $limit = 20
        );

        $data = array();
        $data['pagination'] = $pagination;

        return $this->render('NiqabApplicationBundle:Votes:all.html.twig', $data);
    }

    public function addPollAction()
    {
        $voteId = (int) $this->getRequest()->get('vote_id');
        $answerId = (int) $this->getRequest()->get('vote_answer');

        $vote = $this->getDoctrine()->getRepository('SCVotesBundle:Votes')->findOneById($voteId);

        if (is_null($vote)) {
            return new JsonResponse('nooo');
        }

        foreach ($vote->getAnswers() as $answer) {
            if ($answer->getId() == $answerId) {
                $answer->setCount($answer->getCount() + 1);
                $this->getDoctrine()->getManager()->persist($answer);
                $this->getDoctrine()->getManager()->flush();
                $response = new Response();
                $response->headers->setCookie(new Cookie('voted', $vote->getId()));
            }
        }

        return $this->render('NiqabApplicationBundle:Widget:poll.html.twig', array('vote' => $vote), $response);
    }
}