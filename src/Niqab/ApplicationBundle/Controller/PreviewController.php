<?php

namespace Niqab\ApplicationBundle\Controller;

use SC\ArticleBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/*
 * TODO: переделать на хелпер к твигу
 */
class PreviewController extends Controller
{
    public function getUrlAction (Article $article, $typeId = \SC\ArticleBundle\Preview\Types::SQUARE)
    {
        $previews = $article->getPreviews();

        if (isset($previews[$typeId])) {
            $preview = $previews[$typeId]->getUrl();
        } else {
            $preview =  '0.gif';
        }

        return $this->render('NiqabApplicationBundle:Preview:get.html.twig', array('preview' => $preview));
    }
}
