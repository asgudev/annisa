<?php

namespace Niqab\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends Controller
{
    public function recentAction()
    {
        $authors = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findById([39, 40, 41, 86, 87, 88]);
        $blogCat = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(38);

        return $this->render('NiqabApplicationBundle:Blog:recent.html.twig', ['authors' => $authors, 'category' => $blogCat]);
    }

    public function singleAuthorAction()
    {
        $em = $this->getDoctrine()->getManager();

        $blog_path = $this->getRequest()->get('blog_path');
        $category = $em->getRepository('SCCategoryBundle:Category')->findOneBy(array('path' => $blog_path));

        if (is_null($category)) {
            return new Response('нет такого блога', 404);
        }

        $criteria = array(
            //'isVisibleOnMainPage' => true,
            'isPublished' => true,
            //'secondary_categories' => $category
        );

        $order = array('publishedAt' => 'DESC');
        $articles = $em->getRepository('SCArticleBundle:Article')->findBy($criteria, $order);


        $sortedArticles = array();
        foreach ($articles as $article) {
            if ($article->getSecondaryCategories()[0] == $category) {
                $sortedArticles[$article->getId()] = $article;
            }
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $sortedArticles,
            $this->getRequest()->get('page', 1),
            $limit = 9
        );

        $this->container->get('sc_article_item.service')->hydratePreviews($articles);

        $data = array();
        $data['category'] = $category;
        $data['pagination'] = $pagination;

        return $this->render('NiqabApplicationBundle:Blog:singleAuthor.html.twig', $data);
    }

    public function singleArticleAction($blog_path, $article_id, $article_title)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('SCCategoryBundle:Category')->findOneBy(array('path' => $blog_path));

        if (!$category) {
            throw $this->createNotFoundException('Категория не найдена.');
        }

        $criteria = array(
            'id' => $article_id,
            'isPublished' => true
        );
        $article = $em->getRepository('SCArticleBundle:Article')->findOneBy($criteria);

        if (!$article) {
            throw $this->createNotFoundException('Статья не найдена');
        }

        $this->container->get('sc_article_item.service')->hydratePreviews(array($article));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($article));

        if (mb_strlen($article->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($article);
        }

        $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($article);
        $viewCnt->setCnt($viewCnt->getCnt() + 1);
        $this->getDoctrine()->getManager()->persist($viewCnt);
        $this->getDoctrine()->getManager()->flush();

        return $this->render('NiqabApplicationBundle:Blog:single_article.html.twig', array('article' => $article,
            'articles' => array($article), 'category' => $category));
    }
}