<?php

namespace Niqab\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{
    public function recentAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('SCArticleBundle:Article')->findBy([
            'isPublished' => true,
        ],[
            'publishedAt' => 'DESC'
        ]);


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $request->get('page', 1),
            $limit = 9
        );

        $this->container->get('sc_article_item.service')->hydratePreviews($articles);

        $data = array();
        $data['pagination'] = $pagination;

        return $this->render('NiqabApplicationBundle:Article:recentArticles.html.twig', $data);
    }

    public function showSingleArticleAction($category_path, $sub_category_path = '', $article_id, $article_title)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('SCCategoryBundle:Category')->findOneBy(array('path' => $category_path));

        if (!$category) {
            throw $this->createNotFoundException('Категория не найдена.');
        }

        $criteria = array(
            'id' => $article_id,
            //'alias' => $article_title,
            'primary_category' => $category,
            'isPublished' => true
        );
        $article = $em->getRepository('SCArticleBundle:Article')->findOneBy($criteria);

        if (!$article) {
            throw $this->createNotFoundException('Статья не найдена');
        }

        $this->container->get('sc_article_item.service')->hydratePreviews(array($article));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($article));
        if (!$article->getFlatContent() || ($article->getFlatContent() && mb_strlen($article->getFlatContent()->getFlatContent(), 'utf-8') < 10)) {
//echo '<!-- !!! '. $article->getId() .' -->';
            $this->container->get('sc_article_item.service')->generateFlatContent($article);
        }

        $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($article);
        $viewCnt->setCnt($viewCnt->getCnt() + 1);
        $this->getDoctrine()->getManager()->persist($viewCnt);
        $this->getDoctrine()->getManager()->flush();

        return $this->render('NiqabApplicationBundle:Article:single_article.html.twig', array('article' => $article,
            'articles' => array($article)));
    }


    public function showArticlesInCategoryAction($category_path, $sub_category_path = '')
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('SCCategoryBundle:Category')->findOneBy(array('path' => $category_path));

        if (!$category) {
            throw $this->createNotFoundException('Категория не найдена');
        }


        $subCategory = null;
        if ($sub_category_path != '') {
            $subCategory = $em->getRepository('SCCategoryBundle:Category')->findOneBy(array('path' => $sub_category_path));
        }

        $criteria = array(
            'isPublished' => true,
            'primary_category' => $category
        );

        $order = array('publishedAt' => 'DESC');
        $articles = $em->getRepository('SCArticleBundle:Article')->findBy($criteria, $order);

        $sorted = array();
        if (!is_null($subCategory)) {
            foreach ($articles as $article) {
                if ($article->getSecondaryCategories()[0] == $subCategory) {
                    $sorted[$article->getId()] = $article;
                }
            }
        } else {
            $sorted = $articles;
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $sorted,
            $this->get('request')->query->get('page', 1),
            $limit = 9
        );

        $this->container->get('sc_article_item.service')->hydratePreviews($sorted);

        $data = array();
        $data['category'] = $category;
        $data['sub_category'] = $subCategory;
        $data['pagination'] = $pagination;


        return $this->render('NiqabApplicationBundle:Article:category.html.twig', $data);

    }

    public function searchAction(Request $request)
    {
        $q = $request->get('q');
        $tagPath = $request->get('tag_path');

        $sphinx = $this->get('scsphinx.client');

        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(300, 0);

        $tag = $this->getDoctrine()->getRepository('SCTagBundle:Tag')->findOneByPath($tagPath);

        if (!is_null($tag)) {
            $sphinx->setFilter('tag', [$tag->getId()]);
            $sphinx->q('');
        } else {
            $sphinx->q($q);
        }

        $articles = array();
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy(['id' => $sphinx->getFoundIds()], ['publishedAt' => 'desc']);
            $this->container->get('sc_article_item.service')->hydratePreviews($articles);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $this->get('request')->query->get('page', 1),
            $limit = 30
        );


        $data = array();
        $data['pagination'] = $pagination;

        return $this->render('NiqabApplicationBundle:Article:search.html.twig', $data);


    }

}