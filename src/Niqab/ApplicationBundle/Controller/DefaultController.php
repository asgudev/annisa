<?php

namespace Niqab\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NiqabApplicationBundle:Default:index.html.twig',
            array('page' => $this->get('request')->query->get('page', 1)));
    }

    public function sliderAction()
    {
        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(4, 0);
        $sphinx->sortByDesc('published_at');

        $sphinx->setFilter('is_published', [1]);
        $sphinx->setFilter('is_recommended', [1]);
        //dump($sphinx); die();
        $sphinx->q('');

        $articles = [];
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy(['id' => $sphinx->getFoundIds()], ['publishedAt' => 'desc']);
            $this->container->get('sc_article_item.service')->hydratePreviews($articles);
        }

        return $this->render('NiqabApplicationBundle:Default:slider.html.twig', ['articles' => $articles]);
    }


    public function sendEmailAction(Request $request)
    {
        $name = $request->get('email_name');
        $email = $request->get('email_email');
        $message = $request->get('email_message');

        if (strlen($name) == 0 || strlen($email) == 0 || strlen($message) == 0) {
            return new JsonResponse(array('code' => 'error', 'message' => 'все поля должны быть заполнены'));
        }

        $message = \Swift_Message::newInstance()
            ->setSubject('Новое сообщение[annisa-today.ru]')
            ->setFrom('annisa.today@mail.ru')
            ->setTo('annisa.today@mail.ru')
            ->setBody(
                $this->renderView(
                    'NiqabApplicationBundle:Default:email.txt.twig',
                    array('name' => $name, 'email' => $email, 'message' => $message)
                )
            );
        $this->get('mailer')->send($message);


        return new JsonResponse(array('code' => 'ok'));
    }

    public function rssAction()
    {
        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(25, 0);
        $sphinx->sortByDesc('published_at');

        $sphinx->setFilter('is_published', array(1));

        $sphinx->q('');


        $articles = array();

        if (sizeof($sphinx->getFoundIds()) > 0) {
            $parameters = array('id' => $sphinx->getFoundIds());

            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy($parameters, ['publishedAt' => 'desc']);
        }

        $this->container->get('sc_article_item.service')->hydratePreviews($articles);

        $url = $this->container->getParameter('sc_article.hostname.url');

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml');

        return $this->render('NiqabApplicationBundle:Default:rss.html.twig', ['articles' => $articles, 'url' => $url],
            $response);
    }

}
