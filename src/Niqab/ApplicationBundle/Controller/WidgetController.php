<?php

namespace Niqab\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class WidgetController extends Controller
{
    public function radioAction()
    {
        return $this->render('NiqabApplicationBundle:Widget:radio.html.twig');
    }

    public function inBlogAction()
    {
        $sphinx = $this->get('scsphinx.client');

        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(1000, 0);

        $blogs = [39, 40, 41, 86, 87, 88];
        $sphinx->setFilter('secondcat', $blogs);

        $sphinx->setFilter('is_published', array(1));
        $sphinx->SetFilterRange('published_at', 1351606400, time());
        $sphinx->q('');

        $sphinx->q('');
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $parameters = array('id' => $sphinx->getFoundIds());
            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy($parameters, ['publishedAt' => 'desc']);
        }

        $output = [];
        foreach ($articles as $article) {
                $categoryId = $article->getSecondaryCategories()[0]->getId();

            if (in_array($categoryId, $blogs)) {
                    $output[] = $article;
                    $key = array_search($categoryId, $blogs);
                    unset($blogs[$key]);
                }
        }
        $articles = $output;

        return $this->render('NiqabApplicationBundle:Widget:in_blog.html.twig', array(
            'articles' => $articles,
        ));
    }

    public function pollAction()
    {
        $voted = $this->getRequest()->cookies->get('voted');
        $vote = $this->getDoctrine()->getRepository('SCVotesBundle:Votes')->findOneByIsActive(true);

        /*
            todo: если нет активного голосования, то надо показывать результатыы последнего
        */

        if (!is_null($vote) && $voted == $vote->getId()) {
            return $this->render('NiqabApplicationBundle:Widget:poll.html.twig', array('vote' => $vote));
        } elseif (!is_null($vote)) {
            return $this->render('NiqabApplicationBundle:Widget:active_poll.html.twig', array('vote' => $vote));
        } else {
            return new Response();
        }
    }
}