<?php

namespace SC\UserBundle\Provider;


use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use SC\UserBundle\Entity\BaseUser;
use SC\UserBundle\Entity\SocialUser;
use SC\UserBundle\Provider\OAuthUser;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class Provider extends OAuthUserProvider
{
    protected $session, $doctrine;

    public function __construct($session, $doctrine)
    {
        $this->session = $session;
        $this->doctrine = $doctrine;
    }

    public function loadUserByUsername($username)
    {
        return new OAuthUser($username); //look at the class below
    }

    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $provId = \SC\UserBundle\Provider\Types::getProviderId($response->getResourceOwner()->getName());

        if (is_null($provId)) {
            throw new \Exception('нет такого провайдера');
        }

        if ($provId == \SC\UserBundle\Provider\Types::PROV_NATIVE) {
            $user = $this->doctrine->getRepository('SCUserBundle:BaseUser')->findOneBy(['username' => $response->getUsername()]);
            ld($user);
            exit;
        }

        //data from facebook response
        $id = $response->getUsername();
        $nickname = $response->getNickname();
        $realname = $response->getRealName();

        $nickname = !is_null($nickname) ? $nickname : $realname;
        $email = $response->getEmail();
        $email = !is_null($email) ? $email : $response->getResourceOwner()->getName() . '_' . $id . '@example.com';
        $avatar = $response->getProfilePicture();

        //set data in session
        $this->session->set('nickname', $nickname);
        $this->session->set('realname', $realname);
        $this->session->set('email', $email);
        $this->session->set('avatar', $avatar);


        //1. пытаемся найти такого пользователя
        $socUser = $this->doctrine->getRepository('SCUserBundle:SocialUser')->findOneBy(['socialId' => $id, 'socialType' => $provId]);

        if (is_null($socUser)) {
            $user = new BaseUser();
            $user->setCreatedAt(new \DateTime());
            $user->setEmail($email);
            $user->setSalt(md5(time()));

            // шифрует и устанавливает пароль для пользователя,
            // эти настройки совпадают с конфигурационными файлами
            $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
            $password = $encoder->encodePassword(md5(time() . $nickname), $user->getSalt());
            $user->setPassword($password);
            $user->setScreenName($realname);
            $user->setUsername($nickname);
            $em = $this->doctrine->getManager();

            $em->persist($user);
            $em->flush();

            $socUser = new SocialUser();
            $socUser->setSocialId($id);
            $socUser->setSocialType($provId);
            $socUser->setUser($user);

            $em->persist($socUser);
            $em->flush();


        } else {
            $user = $this->doctrine->getRepository('SCUserBundle:BaseUser')->findOneById($socUser->getUser());
        }

        //set id
        $this->session->set('user_id', $user->getId());

        //@TODO: hmm : is admin
//        if ($this->isUserAdmin($nickname)) {
//            $this->session->set('is_admin', true);
//        }

        //parent:: returned value
        return $user;
        //return $this->loadUserByUsername($response->getNickname());
    }

    public function supportsClass($class)
    {
        return $class === 'Acme\\DemoBundle\\Provider\\OAuthUser';
    }
}