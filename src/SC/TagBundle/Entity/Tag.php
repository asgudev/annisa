<?php

namespace SC\TagBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_tags")
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $title;

    /**
     * @ORM\Column(type="integer")
     */
    protected $grId;

    /**
     * @ORM\Column(type="text")
     */
    protected $path;

    /**
     * @ORM\Column(type="integer")
     */
    protected $level;

    /**
     * @var
     * @ORM\ManyToMany(targetEntity="SC\ArticleBundle\Entity\Article", mappedBy="tags")
     */
    protected $tags_articles;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Tag
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set grId
     *
     * @param integer $grId
     * @return Tag
     */
    public function setGrId($grId)
    {
        $this->grId = $grId;
    
        return $this;
    }

    /**
     * Get grId
     *
     * @return integer 
     */
    public function getGrId()
    {
        return $this->grId;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Tag
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Tag
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }
}