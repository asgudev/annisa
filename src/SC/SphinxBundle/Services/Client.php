<?php
namespace SC\SphinxBundle\Services;
require_once(dirname(__FILE__) . "/sphinxapi.php");

class Client
{
    const CRITERIAS_NAMESPACE_PREFIX = null;

    /**
     * @var SphinxClient
     */
    protected $sphinx = null;
    protected $index_name = null;
    protected $results = null;
    protected $results_ids = array();
    protected $final_results = array();
    protected $shownItems = 0;
    public $starting = 0;
    public $ending = 0;
    public $total = 0;
    const INDEXES = null;
    protected $queryString = '';
    protected $sqlString = null;

    protected $initedCriterias = array();

    protected $forceSearch = false;

    protected $weights = array();

    protected $options;


    protected $host = 'localhost';
    protected $port = 3312;
    protected $indexcmd = '/usr/local/sphinx/bin/indexer -c /my-dev/sphinx/etc/sphinx-cfg.php';

    public function updateIndexes($indexName)
    {
        exec(
            $this->indexcmd . " " . $indexName
            . " --rotate > /dev/null 2>&1 & echo $!", $output);

    }

    public function getForceSearch()
    {
        return $this->forceSearch;
    }

    /**
     * Если установить в true, то поиск будет происходить даже когда нет ни одного критерия
     * @param bool $flag
     */
    public function forceSearch($flag)
    {
        $this->forceSearch = $flag;
        return $this;
    }

    public function getQueryString()
    {
        return $this->queryString;
    }

    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;
    }

    public function setQueryData(array $queryData)
    {
        if (!is_null(static::CRITERIAS_NAMESPACE_PREFIX)) {
            $prefix = static::CRITERIAS_NAMESPACE_PREFIX;
        } else {
            $className = explode('\\', get_class($this));
            $prefix = implode('\\', array_splice($className, 0, -1));
        }
        foreach (static::$criterias as $criteriaName) {
            $critClassName = $prefix . "\\Criteria\\" . $criteriaName;

            $o = new $critClassName();
            /* @var $o CriteriaInterface */

            if ($o->setQueryData($queryData)) {
                $this->initedCriterias[$criteriaName] = $o;
            }
        }
        return $this;
    }

    public function setIndexes($indexes)
    {
        $this->index_name = $indexes;
    }

    public function getIndexes()
    {
        if (is_null($this->index_name)) {
            $this->index_name = static::INDEXES;
        }

        if (is_null($this->index_name)) {
            throw new \Exception('индексы кто будет прописывать?');
        }

        return $this->index_name;

    }

    public function __construct($host, $port, $indexerbincmd, $options = array())
    {
        $this->host = $host;
        $this->port = (int)$port;
        $this->indexcmd = $indexerbincmd;
        $this->sphinx = new \SphinxClient();
        $this->options = $options;
        $this->_init();

    }

    protected function _init()
    {
        $this->sphinx->SetServer($this->host, $this->port);
        $this->sphinx->SetMatchMode(SPH_MATCH_ALL);
        $this->sphinx->SetRankingMode(SPH_RANK_NONE);
        $this->sphinx->SetArrayResult(true);
        $this->resetFilters();
    }

    public function getInitedCriterias()
    {
        return $this->initedCriterias;
    }

    public function setInitedCriterias($initedCriterias)
    {
        $this->initedCriterias = $initedCriterias;
    }

    public function setSelect($select)
    {
        return $this->sphinx->SetSelect($select);
    }

    /// get last error message (string)
    public function getLastError()
    {
        return $this->sphinx->getLastError();
    }

    /// get last warning message (string)
    public function getLastWarning()
    {
        return $this->sphinx->GetLastWarning();
    }

    /**
     * @return SphinxClient
     */
    public function getSphinx()
    {
        return $this->sphinx;
    }

    public function updateAttributes(array $attrs, array $values, $mva = false)
    {
        return $this->sphinx
            ->UpdateAttributes($this->getIndexes(), $attrs, $values, $mva);
    }

    /**
     * По умолчанию мы будем всегда фильтровать deleted,
     * так как это единственный способ удалять элементы из индекса сфинкса до
     * полной переиндексации
     *
     * ВАЖНО: все наши индексы должны поддерживать аттрибут deleted
     */
    public function resetFilters()
    {
        $this->sphinx->ResetFilters();
        $this->sphinx->SetFilter('deleted', array(1), true); // всегда исключаем удаленные
    }

    public function setLimit($limit = 100, $offset = 0)
    {
        $this->sphinx
            ->SetLimits(intval($offset), intval($limit),
                intval($limit + $offset), 0);
        $this->starting = $offset + 1;
        return $this;
    }

    public function setSortMode($sort_mode, $sort_by = '')
    {
        $this->sphinx->SetSortMode($sort_mode, $sort_by);
        return $this;
    }

    public function setGroupBy($attribute, $func, $groupsort = '@group desc')
    {
        $this->sphinx->setGroupBy($attribute, $func, $groupsort);
        return $this;
    }

    public function setMatchMode($match_mode)
    {
        $this->sphinx->SetMatchMode($match_mode);
        return $this;
    }

    public function setRankingMode($ranking_mode)
    {
        $this->sphinx->SetRankingMode($ranking_mode);
        return $this;
    }

    public function setFieldWeights(array $weights)
    {
        foreach ($weights as $name => $value) {
            $this->weights[$name] = $value;
        }

        return $this;
    }

    public function sortByDesc($attrName)
    {
        $this->sphinx->SetSortMode(SPH_SORT_ATTR_DESC, $attrName);
        return $this;
    }

    public function sortCustom($mode, $sortby)
    {
        $this->sphinx->SetSortMode($mode, $sortby);
        return $this;
    }

    public function sortByAsc($attrName)
    {
        $this->sphinx->SetSortMode(SPH_SORT_ATTR_ASC, $attrName);
        return $this;
    }

    public function setFilter($attribute, array $value, $exclude = false)
    {
        $this->sphinx->SetFilter($attribute, $value, $exclude);
        return $this;
    }

    public function SetFilterRange($attribute, $min, $max, $exclude = false)
    {
        $this->sphinx
            ->SetFilterRange($attribute, (int)$min, (int)$max,
                (bool)$exclude);
        return $this;
    }

    public function getGroupByValues()
    {
        $groupBys = array();
        if (isset($this->results['matches'])) {
            foreach ($this->results['matches'] as $match) {
                if (isset($match['attrs']['@groupby'],
                    $match['attrs']['@count'])) {
                    $groupBys[$match['attrs']['@groupby']] = $match['attrs']['@count'];
                }
            }
        }
        return $groupBys;
    }

    public function getSqlString()
    {
        return $this->sqlString;
    }

    public function q($queryString = null)
    {
        if (!$this->getForceSearch() && sizeof($this->initedCriterias) == 0
            && is_null($queryString) && $this->queryString == ''
        ) {
            return $this;
        }

        foreach ($this->initedCriterias as $criteriaName => $criteriaObj) {
            $criteriaObj->alterSphinxObject($this);
        }

        /**
         * Внутренний квери стринг нужен для того
         * чтобы иметь возможность с помощью критериев
         * поправить запрос нужным образом
         */
        if (is_null($queryString) && $this->queryString !== '') {
            $queryString = $this->queryString;
        }

        $this->_q((string)$queryString);

        $this->foundIds = $this->getFoundIds();

        $this->totalFound = $this->getTotalFound();

        if (sizeof($this->foundIds) > 0) {
            $this->sqlString = "WHERE id in(" . implode(',', $this->foundIds)
                . ")";
        }

        if (strlen($this->getLastWarning()) != 0 || strlen($this->getLastError()) != 0
        ) {
            throw new \Exception($this->getLastWarning() . "; " . $this->getLastError());
        }

        return $this;
    }

    protected function _q($query_string)
    {
        if (sizeof($this->weights) > 0) {
            $this->sphinx->SetFieldWeights($this->weights);
        }

        $res = $this->sphinx
            ->Query($this->sphinx->EscapeString($query_string),
                $this->getIndexes());

        $this->results = $res;

        if ($res !== false && isset($res['matches'])
            && sizeof($res['matches']) > 0
        ) {
            $this->ending = $this->starting + sizeof($res['matches']) - 1;
            $this->total = $res['total_found'];
            $this->results = $res;
            $this->shownItems = sizeof($res['matches']);
            $this->processFoundItems();
            return true;
        } else {
            return false;
        }
    }

    public function getSphinxResults()
    {
        return $this->results;
    }

    public function getShownItemsCount()
    {
        return $this->shownItems;
    }

    public function getFoundIds()
    {
        return $this->results_ids;
    }

    public function getTotalFound()
    {
        return $this->total;
    }

    public function getWords()
    {
        if (isset($this->results['words'])) {
            return $this->results['words'];
        } else {
            return array();
        }
    }

    public function addResultFields(array $fields)
    {
        $this->final_results = array();

        foreach ($this->results['matches'] as $match_item) {
            $id = $match_item['id'];
            $this->final_results[$id] = $fields[$id];
            $this->final_results[$id]['attrs'] = $match_item['attrs'];
        }

    }

    public function getFinalResults()
    {
        return $this->final_results;
    }

    protected function processFoundItems()
    {
        $this->results_ids = array();
        /**
         * Массив сформируется с учетом сортировки результатов поиска
         */
        foreach ($this->results['matches'] as $match_item) {
            $this->results_ids[] = $match_item['id'];
        }
    }
}
