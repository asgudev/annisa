<?php

namespace SC\SphinxBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sc_sphinx');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $this->addIndexerSection($rootNode);
        $this->addSearchdSection($rootNode);


        return $treeBuilder;
    }

    private function addIndexerSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('indexer')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('bin')->defaultValue('/usr/bin/indexer')->end()
            ->end()
            ->end()
            ->end();
    }

    private function addSearchdSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('searchd')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('host')->defaultValue('localhost')->end()
            ->scalarNode('port')->defaultValue('3312')->end()
            ->scalarNode('socket')->defaultNull()->end()
            ->end()
            ->end()
            ->end();
    }
}
