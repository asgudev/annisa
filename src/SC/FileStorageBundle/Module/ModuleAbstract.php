<?php
namespace SC\FileStorageBundle\Module;

abstract class ModuleAbstract
{
    const FILE_STORAGE_MODULE_ID = null;
    const FILE_STORAGE_HR_MODULE_NAME = null;

    public static function getHRModuleName()
    {
        if (is_null(static::FILE_STORAGE_HR_MODULE_NAME)) {
            throw new \Exception('необходимо пользоваться наследниками где правильным образом определена константа FILE_STORAGE_HR_MODULE_NAME');
        }

        return static::FILE_STORAGE_HR_MODULE_NAME;
    }

    public static function getFileStorageModuleId()
    {
        if (is_null(static::FILE_STORAGE_MODULE_ID)) {
            throw new \Exception('необходимо пользоваться наследниками где правильным образом определена константа FILE_STORAGE_MODULE_ID');
        }

        return static::FILE_STORAGE_MODULE_ID;
    }

    public function getFileUrl(\SC\FileStorageBundle\Entity\File $file, $moduleSettings)
    {
        return
            $this->container->getParameter('sc_article.hostname.url') .  $this->container->get('sc_file_storage_module.service')->getItemRelativeWebPath($file->getModuleId(), $file->getItemId()) . $file->getFileName();
    }

    protected static function getSubstitutionHostName(\SC\FileStorageBundle\Entity\File $file, $moduleSettings)
    {
        /**
         * Необходимо иметь ввиду, что этот метод дергается для каждого файла
         * - для каждой превьюхи, для каждого аватара и тд и тп
         */
        $hostName = \G2\FileStorage\Host\Service::getHostName($file->getOriginHostId(), $file->getModuleId());

        if (isset($moduleSettings['substitutions'])) {
            /**
             * FIXME: Можно ускорить работу этого метода, если например проверить все бакенды перед прогулкой по циклам
             * то есть фактически сократить вероятность срабатывания основного цикла поиска
             * Так же возможно это все кешировать (хотя хз может дерганье из кеша будет занимать больше времени)
             */
            $backends = $file->getBackends();

            foreach ($moduleSettings['substitutions'] as $substName => $requiredHosts) {
                $ok = true;
                foreach ($requiredHosts as $idx => $_reqHostName) {
                    $_hostId = \G2\FileStorage\Host\Service::getHostId($_reqHostName, $file->getModuleId());
                    $_hostIsUnsynced = !isset($backends[$_hostId]) || $backends[$_hostId]->getState() != \G2\FileStorage\Host\Service::STATE_SYNCED;
                    if ($_hostIsUnsynced) {
                        /**
                         * Важно - этот код будет создавать в базе данных первый хост из субтитьюшнов, даже если этот хост еще не инициализирован никак!
                         * не надо удивлятся тому, что при заливке файлов в базе появляются новые хосты
                         */
                        $ok = false;
                        break;
                    } else {
                        // считаем все все правильно!
                    }
                }

                if ($ok) {
                    // как только нашли первую подстановку - сразу выходим
                    $hostName = $substName;
                    break;
                }
            }
        }

        return $hostName;
    }
}