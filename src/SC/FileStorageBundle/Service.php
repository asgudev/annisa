<?php

namespace SC\FileStorageBundle;

use SC\FileStorageBundle\Entity\File;
use \Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;

class Service
{
    protected $entityManager;
    protected $container;

    public function __construct(\Doctrine\ORM\EntityManager $entityManager, $container)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    public function addFileToStorageFromFileUpload($tmpFileName, $targetFileName, $moduleId, $itemId, $modTypeId = null, $modSubTypeId = null)
    {

        /**
         * Этот метод не поддерживает И НЕ ДОЛЖЕН ПОДДЕРЖИВАТЬ создание файлов путем аплода во вложенные папки
         * он предназначен исключительно для заливки файлов POST ом
         * Если нужно во вложенную попку пользуйтесь G2\FileStorage\File\Service::createObjFromLocalFile
         * разумеется предварительно необходимо вручную файл переместить туда, куда нужно!
         */

        {
            /**
             * Фильтрация имени файла
             */
            // разрешаем только алфавитные символы, и: .-_
            $_targetFileName = preg_replace("/[^A-Za-z0-9-_\\.]/", "", $targetFileName);

            if (!preg_match('/[a-zA-Z0-9]{1,200}\.[a-zA-Z0-9]{1,10}/', $_targetFileName)) {
                // Такая проверка недопускает заливка файлов без расширения - это правильно!
                // Так как метод создает файлы сразу из апдлода
                // FIXME: тут вроде в регулярке нет ссылки к дефисам и подчеркиваниям, но все равно пропускает эти символы!
                // если файл не попадает под наши представленя о корректном имени файла мы должны бросить эксепшн
                throw new \Exception('Illegal characters in filename: ' . $targetFileName);
            } else {
                $targetFileName = $_targetFileName;
            }
        }

        // получаем список файлов
        $itemFiles = $this->entityManager->getRepository('SCFileStorageBundle:File')
            ->findById($itemId);


        foreach ($itemFiles as $fileId => $fileObj) {
            if ($fileObj->getFileName() == $targetFileName) {
                /**
                 * ВАЖНО! если файл с таким именем уже существует, то можно его же и отдать -
                 * пусть юзеры сначала удалят существующий файл с таким же именем
                 */
                return $fileObj;
            }
        }


        $localUploadPath = $this->container->get('sc_file_storage_module.service')->getModuleLocalUploadPath($moduleId, $itemId);
        if (!file_exists($localUploadPath)) {
            mkdir($localUploadPath, 0777, true);
        }

        $fullFileName = $localUploadPath . '/' . $targetFileName;

        if (move_uploaded_file($tmpFileName, $fullFileName)) {

            $file = $this->container->get('sc_file_storage_file.service')->createObjFromLocalFile($fullFileName, $itemId, $moduleId, $modTypeId, $modSubTypeId);

            $this->entityManager->persist($file);
            $this->entityManager->flush();

            return $file;
        } else {
            throw new \Exception('не удалось переместить файл в методе ' . __CLASS__ . '::' . __METHOD__);
        }
    }

    public function getFilesFor($moduleId, array $itemIds, $modTypeId = null, $modSubTypeId = null)
    {

        $criteria = array(
            'itemId' => $itemIds,
            //'moduleId' => $moduleId
        );

        if (! is_null($modTypeId)) {
            $criteria['modTypeId'] = (array) $modTypeId;
        }

        if (! is_null($modSubTypeId)) {
            $criteria['modSubTypeId'] = (array) $modSubTypeId;
        }

        $order = array('fileName' => 'ASC');
        $files = $this->entityManager->getRepository('SCFileStorageBundle:File')->findBy($criteria, $order);

        $outFiles = array();

        foreach ($files as $file) {
            $outFiles[$file->getId()] = $file;
            $file->setModuleId($moduleId);
            $file->setUrl($this->container->get('sc_file_storage_module.service')->getFileUrl($file->getModuleId(), $file));
        }

        return $outFiles;
    }

    public function saveFile (\SC\FileStorageBundle\Entity\File $file)
    {
        /**
         * FIXME: проверить консистентность данных перед запихиванием в базу
         * размер, дата создания и модификации не должны быть нулевыми!
         *
         */

        if (is_null($file->getId())) {
            /**
             * Превентивная проверка имени файла на валидность
             * FIXME: вынести сюда все проверки имени файла, сейчас они разбросаны по коду
             * Унести это добро в файлаплодер - наша система не должна знать об этом
             */
//                if (! mb_detect_encoding($file->getFileName(), array(
//                    'UTF-8'
//                ), true)) {
//                    throw new \Exception('fileName is in invalid encoding, only UTF-8 is allowed');
//                }


            $this->entityManager->persist($file);
            $this->entityManager->flush();

        } else {

        }



        return $file;
    }

    public function deleteFile (\SC\FileStorageBundle\Entity\File $file)
    {
        {
            /**
             * В силу того, что в данный момент наш слой работы Db не поддерживает
             * почеловечески транзакций, и к томуже не умеет бросать эксепшны на ошибках
             * мы вынуждены отложить завертывание этого метода в транзакцию
             */

            $this->entityManager->remove($file);
            $this->entityManager->flush();

            $localFilePath = $this->container->get('sc_file_storage_module.service')->getModuleLocalUploadPath($file->getModuleId(), $file->getItemId()) . '/' . $file->getFileName();
            if (file_exists($localFilePath)) {
                unlink($localFilePath);
            }
        }

        return true;
    }
}