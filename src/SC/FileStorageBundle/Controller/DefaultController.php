<?php

namespace SC\FileStorageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SCFileStorageBundle:Default:index.html.twig', array('name' => $name));
    }
}
