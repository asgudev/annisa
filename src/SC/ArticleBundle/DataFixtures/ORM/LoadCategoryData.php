<?php

namespace SC\ArticleBundle\DataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SC\ArticleBundle\Entity\Category;

class LoadCategoryData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        return;


        $data = Array(
            'politics',
            'economy',
            'community',
            'auto',
            'sport',
            'science',
            'culture',
            'medicine',
            'real-estate',
            'tech',
            array('personality', null, 'false'),
            array('monitoring_smi', null, 'false')

        );
        for ($i = 0; $i < count($data); $i++) {
            if (is_array($data[$i])) {
                $alias = $data[$i][0];
                $order = $data[$i][1];
                $primary = $data[$i][2];
            } else {
                // default
                $alias = $data[$i];
                $order = $i + 1;
                $primary = true;
            }

            $category = new Category();
            $category->setAlias($alias);
            $category->setOrder($order);
            $category->setPrimaryCategory($primary);
            $manager->persist($category);
            $manager->flush();
        }
    }
}