<?php

namespace SC\ArticleBundle\Repositories;

use Doctrine\ORM\EntityRepository;

class ArticleRepository extends EntityRepository
{
    public function findBySecondaryCategories(array $categories)
    {
        return $this->createQueryBuilder('b')
            ->join('b.secondary_categories', 'p')
            ->where('p.id in (:secondary_categories)')
            ->andWhere("b.isPublished = 1")
            //->groupBy("b.secondary_categories")
            ->orderBy("b.publishedAt", "DESC")
            ->setParameter('secondary_categories', $categories)
            ->getQuery()
            ->getResult();
    }

    public function findSingleBySecondaryCategories(array $categories)
    {
        $articles = [];
        foreach ($categories as $cat) {
            $article = $this->createQueryBuilder('b')
                ->join('b.secondary_categories', 'p')
                ->where('p.id = (:category)')
                ->andWhere("b.isPublished = 1")
                //->groupBy("b.secondary_categories")
                ->orderBy("b.publishedAt", "DESC")
                ->setParameter('category', $cat)
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();
            if (!empty($article) ) {
                $articles[] = $article[0];
            }
        }

        return $articles;
    }


    public function findByTags(array $tags)
    {
        return $this->createQueryBuilder('b')
            ->join('b.tags', 'p')
            ->where('p.id in (:tags) and b.isPublished = 1 ORDER BY b.publishedAt DESC')
            ->setParameter('tags', $tags)
            ->getQuery()
            ->getResult();
    }

    public function findByFiles(array $files)
    {
        return $this->createQueryBuilder('b')
            ->join('b.files', 'p')
            ->where('p.id in (:files) and b.isPublished = 1 ORDER BY b.publishedAt DESC')
            ->setParameter('files', $files)
            ->getQuery()
            ->getResult();
    }
}
