<?php

namespace SC\ArticleBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_article_content")
 */
class ArticleContent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", name="content")
     */
    protected $content = '';

    /**
     * @ORM\OneToOne(targetEntity="Article", inversedBy="content")
     * @ORM\JoinColumn(name="article", referencedColumnName="id")
     */
    protected $article;

    /**
     * @ORM\Column(type="text", name="typo_content")
     */
    protected $typographedContent = '';

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return ArticleContent
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set typographedContent
     *
     * @param string $typographedContent
     * @return ArticleContent
     */
    public function setTypographedContent($typographedContent)
    {
        $this->typographedContent = $typographedContent;
    
        return $this;
    }

    /**
     * Get typographedContent
     *
     * @return string 
     */
    public function getTypographedContent()
    {
        return $this->typographedContent;
    }

    /**
     * Set article
     *
     * @param \SC\ArticleBundle\Entity\Article $article
     * @return ArticleContent
     */
    public function setArticle(\SC\ArticleBundle\Entity\Article $article = null)
    {
        $this->article = $article;
    
        return $this;
    }

    /**
     * Get article
     *
     * @return \SC\ArticleBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}