<?php

namespace SC\ArticleBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_article_stats")
 */
class ArticleStats
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Article", inversedBy="viewCnt")
     */
    protected $article;

    /**
     * @ORM\Column(type="integer")
     */
    protected $cnt = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cnt
     *
     * @param integer $cnt
     * @return GalleryViewCnt
     */
    public function setCnt($cnt)
    {
        $this->cnt = $cnt;

        return $this;
    }

    /**
     * Get cnt
     *
     * @return integer
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * Set article
     *
     * @param \SC\ArticleBundle\Entity\Article $article
     * @return ArticleStats
     */
    public function setArticle(\SC\ArticleBundle\Entity\Article $article = null)
    {
        $this->article = $article;
    
        return $this;
    }

    /**
     * Get article
     *
     * @return \SC\ArticleBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}