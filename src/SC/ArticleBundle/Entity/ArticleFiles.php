<?php

namespace SC\ArticleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="sc_article_files")
 */
class ArticleFiles {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $itemId;


    /**
     * @ORM\Column(type="integer")
     */
    protected $fileItemId;


    /**
     * @ORM\Column(type="integer")
     */
    protected $fileId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     * @return ArticleFiles
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    
        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set fileItemId
     *
     * @param integer $fileItemId
     * @return ArticleFiles
     */
    public function setFileItemId($fileItemId)
    {
        $this->fileItemId = $fileItemId;
    
        return $this;
    }

    /**
     * Get fileItemId
     *
     * @return integer 
     */
    public function getFileItemId()
    {
        return $this->fileItemId;
    }

    /**
     * Set fileId
     *
     * @param integer $fileId
     * @return ArticleFiles
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
    
        return $this;
    }

    /**
     * Get fileId
     *
     * @return integer 
     */
    public function getFileId()
    {
        return $this->fileId;
    }
}