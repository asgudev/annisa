<?php

namespace SC\ArticleBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use SC\ArticleBundle\Entity\ArticleFlatContent;
use SC\ArticleBundle\Entity\ArticleContent;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="SC\ArticleBundle\Repositories\ArticleRepository")
 * @ORM\Table(name="sc_article")
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="lastmodyfied_at")
     *
     * @var DateTime $lastModyfiedAt
     */
    protected $lastModyfiedAt;

    /**
     * @ORM\Column(type="datetime", name="published_at")
     *
     * @var DateTime $publishedAt
     */
    protected $publishedAt;

    /**
     * @ORM\Column(type="integer")
     */
    protected $authorId;

    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    protected $isPublished;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title;

    /**
     * @ORM\Column(type="text", name="short_anounce")
     */
    protected $shortAnounce;

    /**
     * @ORM\Column(type="text", name="long_anounce")
     */
    protected $longAnounce;

    /**
     * @ORM\Column(type="boolean", name="is_visible_on_main_page")
     */
    protected $isVisibleOnMainPage = 1;

    /**
     * @ORM\Column(type="integer", name="comment_output_style")
     */
    protected $commentOutPutStyle = null;

    /**
     * @ORM\Column(type="boolean", name="priority")
     */
    protected $priority = 0;

    /**
     * @ORM\Column(type="boolean", name="is_recommended")
     */
    protected $isRecommeded = 0;

    /**
     * @ORM\Column(type="boolean", name="is_for_yandex")
     */
    protected $isForYandex = 0;


    /**
     * @ORM\OneToOne(targetEntity="ArticleContent", mappedBy="article", cascade={"persist", "remove"})
     */
    protected $content;

    /**
     * @ORM\OneToOne(targetEntity="ArticleFlatContent", mappedBy="article", cascade={"persist", "remove"})
     */
    protected $flatContent;


    /**
     * Основная категория
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="\SC\CategoryBundle\Entity\Category", inversedBy="primary_articles", cascade={"persist"})
     * @ORM\JoinColumn(name="primary_category_id", referencedColumnName="id")
     */
    private $primary_category;

    /**
     * Вторичные категории
     * @var \Category
     *
     * @ORM\ManyToMany(targetEntity="\SC\CategoryBundle\Entity\Category", inversedBy="secondary_articles", cascade={"persist"})
     * @ORM\JoinColumn(name="secondary_categories_id", referencedColumnName="id")
     *
     */
    private $secondary_categories;

    /**
     * Теги
     * @var \Tag
     *
     * @ORM\ManyToMany(targetEntity="\SC\TagBundle\Entity\Tag", inversedBy="tags_articles")
     * @ORM\JoinColumn(name="tags_id", referencedColumnName="id")
     *
     */
    protected $tags;


    protected $previews;

    protected $moduleId;

    protected $files;

    /**
     * @ORM\ManyToOne(targetEntity="\SC\UserBundle\Entity\BaseUser", inversedBy="article")
     */
    protected $author;

    /**
     * @ORM\Column(type="boolean", name="super_main")
     */
    protected $superMain = 0;


    /**
     * @ORM\Column(type="boolean", name="main_left")
     */
    protected $mainLeft = 0;


    /**
     * @ORM\Column(type="boolean", name="main_right")
     */
    protected $mainRight = 0;

    /**
     * @ORM\OneToOne(targetEntity="ArticleMeta", cascade={"persist", "remove"})
     */
    protected $meta;

    /**
     * @var int
     *
     * @ORM\Column(name="comments_count", type="integer", nullable=true)
     */
    protected $commentsCount = 0;


    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $originLinkTitle = '';

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $originLinkLink = '';

    /**
     * @ORM\Column(type="integer")
     */
    protected $letter = 0;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $path;

    /**
     * @ORM\OneToOne(targetEntity="ArticleStats", mappedBy="article", cascade={"persist", "remove"})
     */
    protected $viewCnt;

    /**
     * @ORM\Column(type="integer")
     */

    protected $oldArticleId = 0;

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public function setFiles($files)
    {
        $this->files = $files;
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function setModuleId($moduleId)
    {
        $this->moduleId = $moduleId;
        return $this;
    }

    public function getModuleId()
    {
        return $this->moduleId;
    }

    public function getPreviews()
    {
        return $this->previews;
    }

    public function setPreviews($previews)
    {
        $this->previews = $previews;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Article
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set lastModyfiedAt
     *
     * @param \DateTime $lastModyfiedAt
     * @return Article
     */
    public function setLastModyfiedAt($lastModyfiedAt)
    {
        $this->lastModyfiedAt = $lastModyfiedAt;

        return $this;
    }

    /**
     * Get lastModyfiedAt
     *
     * @return \DateTime
     */
    public function getLastModyfiedAt()
    {
        return $this->lastModyfiedAt;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     * @return Article
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     * @return Article
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     * @return Article
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set shortAnounce
     *
     * @param string $shortAnounce
     * @return Article
     */
    public function setShortAnounce($shortAnounce)
    {
        $this->shortAnounce = $shortAnounce;

        return $this;
    }

    /**
     * Get shortAnounce
     *
     * @return string
     */
    public function getShortAnounce()
    {
        return $this->shortAnounce;
    }

    /**
     * Set longAnounce
     *
     * @param string $longAnounce
     * @return Article
     */
    public function setLongAnounce($longAnounce)
    {
        $this->longAnounce = $longAnounce;

        return $this;
    }

    /**
     * Get longAnounce
     *
     * @return string
     */
    public function getLongAnounce()
    {
        return $this->longAnounce;
    }

    /**
     * Set isVisibleOnMainPage
     *
     * @param boolean $isVisibleOnMainPage
     * @return Article
     */
    public function setIsVisibleOnMainPage($isVisibleOnMainPage)
    {
        $this->isVisibleOnMainPage = $isVisibleOnMainPage;

        return $this;
    }

    /**
     * Get isVisibleOnMainPage
     *
     * @return boolean
     */
    public function getIsVisibleOnMainPage()
    {
        return $this->isVisibleOnMainPage;
    }

    /**
     * Set commentOutPutStyle
     *
     * @param integer $commentOutPutStyle
     * @return Article
     */
    public function setCommentOutPutStyle($commentOutPutStyle)
    {
        $this->commentOutPutStyle = $commentOutPutStyle;

        return $this;
    }

    /**
     * Get commentOutPutStyle
     *
     * @return integer
     */
    public function getCommentOutPutStyle()
    {
        return $this->commentOutPutStyle;
    }

    /**
     * Set priority
     *
     * @param boolean $priority
     * @return Article
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return boolean
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set isRecommeded
     *
     * @param boolean $isRecommeded
     * @return Article
     */
    public function setIsRecommeded($isRecommeded)
    {
        $this->isRecommeded = $isRecommeded;

        return $this;
    }

    /**
     * Get isRecommeded
     *
     * @return boolean
     */
    public function getIsRecommeded()
    {
        return $this->isRecommeded;
    }

    /**
     * Set isForYandex
     *
     * @param boolean $isForYandex
     * @return Article
     */
    public function setIsForYandex($isForYandex)
    {
        $this->isForYandex = $isForYandex;

        return $this;
    }

    /**
     * Get isForYandex
     *
     * @return boolean
     */
    public function getIsForYandex()
    {
        return $this->isForYandex;
    }

    /**
     * Set content
     *
     * @param \SC\ArticleBundle\Entity\ArticleContent $content
     * @return Article
     */
    public function setContent(\SC\ArticleBundle\Entity\ArticleContent $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return \SC\ArticleBundle\Entity\ArticleContent
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set flatContent
     *
     * @param \SC\ArticleBundle\Entity\ArticleFlatContent $flatContent
     * @return Article
     */
    public function setFlatContent(\SC\ArticleBundle\Entity\ArticleFlatContent $flatContent = null)
    {
        $this->flatContent = $flatContent;

        return $this;
    }

    /**
     * Get flatContent
     *
     * @return \SC\ArticleBundle\Entity\ArticleFlatContent
     */
    public function getFlatContent()
    {
        return $this->flatContent;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->secondary_categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set primary_category
     *
     * @param \SC\CategoryBundle\Entity\Category $primaryCategory
     * @return Article
     */
    public function setPrimaryCategory(\SC\CategoryBundle\Entity\Category $primaryCategory = null)
    {
        $this->primary_category = $primaryCategory;

        return $this;
    }

    /**
     * Get primary_category
     *
     * @return \SC\CategoryBundle\Entity\Category
     */
    public function getPrimaryCategory()
    {
        return $this->primary_category;
    }

    /**
     * Add secondary_categories
     *
     * @param \SC\CategoryBundle\Entity\Category $secondaryCategories
     * @return Article
     */
    public function addSecondaryCategorie(\SC\CategoryBundle\Entity\Category $secondaryCategories)
    {
        $this->secondary_categories[] = $secondaryCategories;

        return $this;
    }

    /**
     * Remove secondary_categories
     *
     * @param \SC\CategoryBundle\Entity\Category $secondaryCategories
     */
    public function removeSecondaryCategorie(\SC\CategoryBundle\Entity\Category $secondaryCategories)
    {
        $this->secondary_categories->removeElement($secondaryCategories);
    }

    /**
     * Get secondary_categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSecondaryCategories()
    {
        return $this->secondary_categories;
    }

    /**
     * Set commentsCount
     *
     * @param integer $commentsCount
     * @return Article
     */
    public function setCommentsCount($commentsCount)
    {
        $this->commentsCount = $commentsCount;

        return $this;
    }

    /**
     * Get commentsCount
     *
     * @return integer
     */
    public function getCommentsCount()
    {
        return $this->commentsCount;
    }


    /**
     * Set originLinkTitle
     *
     * @param string $originLinkTitle
     * @return Article
     */
    public function setOriginLinkTitle($originLinkTitle)
    {
        $this->originLinkTitle = $originLinkTitle;

        return $this;
    }

    /**
     * Get originLinkTitle
     *
     * @return string
     */
    public function getOriginLinkTitle()
    {
        return $this->originLinkTitle;
    }

    /**
     * Set originLinkLink
     *
     * @param string $originLinkLink
     * @return Article
     */
    public function setOriginLinkLink($originLinkLink)
    {
        $this->originLinkLink = $originLinkLink;

        return $this;
    }

    /**
     * Get originLinkLink
     *
     * @return string
     */
    public function getOriginLinkLink()
    {
        return $this->originLinkLink;
    }

    /**
     * Set letter
     *
     * @return Article
     */
    public function setLetter()
    {
        $this->letter = $this->ordutf8(mb_strtolower(mb_substr($this->title, 0, 1, 'utf8'), 'utf8'));

        return $this;
    }

    /**
     * Get letter
     *
     * @return integer
     */
    public function getLetter()
    {
        return $this->letter;
    }

    protected function ordutf8($string, $offset = 0)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) $offset = -1;
        return $code;
    }

    /**
     * Add tags
     *
     * @param \SC\TagBundle\Entity\Tag $tags
     * @return Article
     */
    public function addTag(\SC\TagBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;
    
        return $this;
    }

    /**
     * Remove tags
     *
     * @param \SC\TagBundle\Entity\Tag $tags
     */
    public function removeTag(\SC\TagBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set superMain
     *
     * @param boolean $superMain
     * @return Article
     */
    public function setSuperMain($superMain)
    {
        $this->superMain = $superMain;
    
        return $this;
    }

    /**
     * Get superMain
     *
     * @return boolean 
     */
    public function getSuperMain()
    {
        return $this->superMain;
    }

    /**
     * Set mainLeft
     *
     * @param boolean $mainLeft
     * @return Article
     */
    public function setMainLeft($mainLeft)
    {
        $this->mainLeft = $mainLeft;
    
        return $this;
    }

    /**
     * Get mainLeft
     *
     * @return boolean 
     */
    public function getMainLeft()
    {
        return $this->mainLeft;
    }

    /**
     * Set mainRight
     *
     * @param boolean $mainRight
     * @return Article
     */
    public function setMainRight($mainRight)
    {
        $this->mainRight = $mainRight;
    
        return $this;
    }

    /**
     * Get mainRight
     *
     * @return boolean 
     */
    public function getMainRight()
    {
        return $this->mainRight;
    }

    /**
     * Set meta
     *
     * @param \SC\ArticleBundle\Entity\ArticleMeta $meta
     * @return Article
     */
    public function setMeta(\SC\ArticleBundle\Entity\ArticleMeta $meta = null)
    {
        $this->meta = $meta;
    
        return $this;
    }

    /**
     * Get meta
     *
     * @return \SC\ArticleBundle\Entity\ArticleMeta 
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Article
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set viewCnt
     *
     * @param \SC\ArticleBundle\Entity\ArticleStats $viewCnt
     * @return Article
     */
    public function setViewCnt(\SC\ArticleBundle\Entity\ArticleStats $viewCnt = null)
    {
        $this->viewCnt = $viewCnt;
    
        return $this;
    }

    /**
     * Get viewCnt
     *
     * @return \SC\ArticleBundle\Entity\ArticleStats 
     */
    public function getViewCnt()
    {
        return $this->viewCnt;
    }

    /**
     * Set oldArticleId
     *
     * @param integer $oldArticleId
     * @return Article
     */
    public function setOldArticleId($oldArticleId)
    {
        $this->oldArticleId = $oldArticleId;
    
        return $this;
    }

    /**
     * Get oldArticleId
     *
     * @return integer 
     */
    public function getOldArticleId()
    {
        return $this->oldArticleId;
    }
}