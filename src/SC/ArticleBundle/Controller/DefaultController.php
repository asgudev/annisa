<?php

namespace SC\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SCArticleBundle:Default:index.html.twig', array('name' => $name));
    }
}
