<?php
namespace SC\ArticleBundle\Item\Integrator;

use SC\ArticleBundle\Entity\Article;

/**
 * ВАЖНО! В задачи интегратора так же входит создание и поддержка связей между элементами
 */
class FileStorage
{
    protected $article = null;
    protected $text = '';
    protected $container;

    protected $dependentFilesArticleIds = array();

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function setArticle(Article $article)
    {
        $this->article = $article;
        return $this;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getRenderedContent()
    {
        $content = preg_replace_callback('/(?P<imgTag><img(?:\s\w+="[^"]*")*\sdata-alt="' . \SC\ArticleBundle\Constants::FILE_INTEGRATION_KEY . '"[^>]+>)/ix',
            array(
                $this,
                'renderSingleItem'
            ),
            $this->text);

        {
            $this->container->get('sc_article_item_integration.files')->replace($this->article->getId(), $this->dependentFilesArticleIds);
        }

        return $content;
    }

    protected function renderSingleItem($imgTag)
    {
        if (isset($imgTag['imgTag'])) {
            preg_match('#alt="(.*?)"#mis', $imgTag['imgTag'], $altAttr);
            preg_match('#src="(.*?)"#mis', $imgTag['imgTag'], $srcAttr);
            $alt = isset($altAttr[1]) ? $altAttr[1] : '';
            if (isset($srcAttr[1])) {
                preg_match('#params=(.*)#mis', urldecode($srcAttr[1]), $params);

                if (isset($params[1])) {
                    $settings = (array)json_decode($params[1]);
                    if (is_array($settings) && sizeof($settings) > 0) {
                        return $this->renderFile($settings['moduleId'], $settings['itemId'], $settings['fileId'], (array)$settings['options'], $alt);
                    }
                }
            }
        }

        return '';
    }

    protected function renderFile($moduleId, $itemId, $fileId, $options, $alt = '')
    {

        if ($itemId == $this->article->getId()) {
            // файл из текущей статьи
            $allFiles = $this->article->getFiles();
        } else {
            // файл из другой статьи (видимо редакторы скопировали его откудато)
            $allFiles = $this->container->get('sc_file_storage.service')->getFilesFor($moduleId, (array)$itemId);
            $allFiles = array_shift($allFiles);
        }


        if (!isset($allFiles[$fileId])) {
            /**
             * FIXME: нужный нам файл не найден, лучшее что мы можем сделать - это выйти отсюда с пустым результатом
             * но по хорошему бы как-то сигнализировать либо в лог либо как-то иначе
             */
            return '';
        }

        $fileToInsert = null;
        $fileLinkingTo = null;
        $fileToInsert = $allFiles[$fileId];

        /**
         * ВАЖНО! Сохраняем зависимости от файлов для текущей статьи
         * Помним, что внутри материала могут использоваться файлы из других статей,
         * поэтому сохраняем $fileToInsert->getItemId()
         */
        $this->dependentFilesArticleIds[$fileToInsert->getItemId()][$fileToInsert->getId()] = $fileToInsert->getId();

        if ($options['resize'] == 1) {
            /**
             * если в опциях есть ['previewFileId'] то используем его!
             */
            if (isset($options['previewFileId'])) {
                // FIXME: тут ебанутая логика - я всю голову себе сломал, надо пофиксить
                $fileLinkingTo = $fileToInsert;
                $fileToInsert = $allFiles[$options['previewFileId']];

            } else {
                // надо найти ресайзнутый файл
                $relatedFiles = $this->container->get('sc_file_storage_module.service')->getRelatedFiles($allFiles[$fileId]->getModuleId(), $allFiles, $allFiles[$fileId]->getId());
                $resizeWidth = $options['previewWidth'];
                foreach ($relatedFiles as $relatedFile) {
                    if ($relatedFile->getModSubTypeId() == $resizeWidth) {
                        $fileLinkingTo = $fileToInsert;
                        $fileToInsert = $relatedFile;
                        break;
                    }
                }
            }
        }

        if (!is_null($fileLinkingTo)) {
            /**
             * ВАЖНО! Сохраняем зависимости от файлов для текущей статьи
             * Помним, что внутри материала могут использоваться файлы из других статей,
             * поэтому сохраняем $fileLinkingTo->getItemId()
             */
            $this->dependentFilesArticleIds[$fileLinkingTo->getItemId()][$fileLinkingTo->getId()] = $fileLinkingTo->getId();

            // значит будем рендерить линкованный файл

            $integrationResize = new \SC\ArticleBundle\Controller\IntegrationController();

            return $integrationResize->fileWithResizeAction($fileToInsert, $fileLinkingTo, $options, $this->article, $alt)->getContent();

        } else {

            $integrationResize = new \SC\ArticleBundle\Controller\IntegrationController();
            return $integrationResize->fileWithResizeAction($fileToInsert, $fileToInsert, $options, $this->article, $alt)->getContent();


        }
    }
}