<?php

namespace SC\ArticleBundle\Item\Integration;

use SC\ArticleBundle\Entity\ArticleFiles;

class Files
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function replace($articleId, $dependentFilesArticleIds)
    {

        $this->em->transactional(function ($em) use  ($articleId, $dependentFilesArticleIds) {

            $articleFiles = $this->em->getRepository('SCArticleBundle:ArticleFiles')->findOneByItemId($articleId);

            if (!is_null($articleFiles)) {
                $em->remove($articleFiles);
                $em->flush();
            }


            foreach ($dependentFilesArticleIds as $fileItemId => $filesIds) {
                foreach ($filesIds as $_fid) {
                    $articleFiles = new ArticleFiles();
                    $articleFiles->setItemId($articleId);
                    $articleFiles->setFileItemId($fileItemId);
                    $articleFiles->setFileId($_fid);

                    $em->persist($articleFiles);
                }
                $em->flush();
            }

        });

    }
}