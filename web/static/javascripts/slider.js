(function (window, $, undefined) {
  'use strict';

  $(function () {

    $('[flexslider]').each(function () {
      var self, settings,defaults, flexSettings;

      self = $(this);
      settings = self.data('settings');
      defaults = {
        animation: 'slide',
        slideshow: false,
        animationLoop: true
      }
      flexSettings = $.extend(defaults, settings);

      try {
        self.flexslider(flexSettings);
      } catch (e) {
        console.error(e);
      }
    });

  });

}(window, window.jQuery));