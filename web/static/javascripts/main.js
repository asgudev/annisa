jQuery(function(){
    jQuery('[placeholder]').each(function(){
        inputPlaceholder(jQuery(this)[0]);
    });

    jQuery(window).scroll(function(){
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scrollup').fadeIn();
        } else {
            jQuery('.scrollup').fadeOut();
        }
    });

    jQuery('.scrollup').click(function(){
        jQuery("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });


    jQuery('.js_email_send').on('click', function(ev) {
        ev.preventDefault();
        jQuery('#sendMessageModal').modal('show');
        //console.log(jQuery('#sendMessageModal').modal('show'));
    });

    jQuery('.js_email_form_send_button').on('click', function() {
        var error_message = '';

        if (jQuery('.js_email_message').val().length == 0) {
            error_message = 'Задайте свой вопрос';
        }

        if (jQuery('.js_email_email').val().length == 0) {
            error_message = 'Укажите свой e-mail';
        }

        if (jQuery('.js_email_name').val().length == 0) {
            error_message = 'Представьтесь, пожалуйста';
        }

        if (error_message.length != 0) {
            jQuery('.js_email_error').html(error_message);
            jQuery('.js_email_error_show').show();
            return false;
        }

        var posting = jQuery.post('/email/message/send/', jQuery(".js_send_email_form").serialize());

        posting.done(function(data) {
            if (data.code == 'error') {
                jQuery('.js_email_error').html(data.message);
                jQuery('.js_email_error_show').show();
                return false;
            }

            if (data.code == 'ok') {
                jQuery('.js_email_ok_show').show();
                setTimeout("jQuery('#sendMessageModal').modal('hide')", 1000);
                setTimeout("jQuery('.js_email_ok_show').hide()", 1000);
                return false;
            }

        });

        posting.fail(function(data) {
            console.log(data);
        })

    });


    jQuery('.js_email_error_close').on('click', function(){
        jQuery('.js_email_error').empty();
        jQuery('.js_email_error_show').hide();
    });


    $('.form-submitter').click(function(e){
        e.preventDefault();
        var form =  typeof $(this).data('form') != 'undefined' ? $(this).data('form') : $(this).parents('form');
        $(form).submit();
    });

});