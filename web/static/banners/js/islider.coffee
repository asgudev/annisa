$.widget "ui.iSlider", {
    options:
        width: 100
        slides: []
        template: false
        templateHtml: ''
        speed: 5000
        autoplay: yes
        debug: false
        width: 100
        height: 100
        shadowSize: 4

    totalWidth: 0
    currentSlide: 0
    slidesContainer: null
    slideTimer: 0

    parseTemplate: (data) ->
      templateResult = this.options.templateHtml
      $.each data, (key, value)->
        templateResult = templateResult.replace(new RegExp("{#{ key }}","gm"), value)
        return
      return templateResult

    createHtml: (element) ->
        self = this
        body = $('<div />').addClass 'iSliderBody'
        

        containerWidth = self.options.width+2;
        containerHeight = self.options.height+2+self.options.shadowSize
      
        #body.css {'width':containerWidth, 'height':containerHeight}

        self.slidesContainer = $('<div />').addClass 'iSlidesContainer'
        self.slidesContainer.css {"height":self.options.height+25}
        $(body).append self.slidesContainer
        $(self.slidesContainer).append self.getControls self
        $(element).append(body)

        firstSlide = self.options.slides[0]
        self.fillData firstSlide

        return

    getControls: (parent) ->
        controls = $('<div />').addClass 'iSliderControls'
        next = $('<a />').addClass 'controlNext'
        next.on 'click', () ->
            parent.stopTimer()
            parent.slide true
            return;
        controls.append(next)
        
        prev = $('<a />').addClass 'controlPrev'
        prev.on 'click', () ->
            parent.stopTimer()
            parent.slide false
            return;
        controls.append(prev)
        
        return controls;

    addShadow: (element, color) ->
      $(element).css {
        '--webkit-box-shadow': "0 #{ this.options.shadowSize }px 0 #{ color }"
        '-moz-box-shadow': "0 #{ this.options.shadowSize }px 0 #{ color }"
        'box-shadow': "0 #{ this.options.shadowSize }px 0 #{ color }"
      }
      return

    _create: ->
        self = this
        o = self.options
        el = self.element
        self.createHtml el
        self.startTimer()
        return

    startTimer: () ->
        self = this
        clearInterval(self.slideTimer)
        self.slideTimer = 
            setInterval () ->
                self.slide()
            , self.options.speed
        return;

    stopTimer: () ->
        self = this
        clearInterval(self.slideTimer)
        return;

    slide: (next = true) ->
        self = this
        if next is yes
            self.setNextSlide()
            direction = 1
        else
            self.setPrevSlide()
            direction = -1
        
        nextSlideObject = self.options.slides[self.currentSlide]
        self.switchData nextSlideObject, direction
        return;

    setNextSlide: ->
        self = this
        next = self.currentSlide + 1
        next = if next == self.options.slides.length then 0 else next
        self.currentSlide = next
        return next;

    setPrevSlide: ->
        self = this
        next = self.currentSlide - 1
        next = if next == -1 then self.options.slides.length-1 else next
        self.currentSlide = next
        return next;

    switchData: (slide, direction) ->
        self = this
        sign = if direction > 0 then "+" else "-"
        antisign = if direction > 0 then "-" else "+"
        $(self.slidesContainer)
            .transition 
                perspective: '800px'
                rotateY: "#{sign}=90deg"
                ,250,'easeInBack', () ->
                    $('.iSlide',this).remove()
                    self.fillData(slide)
                    this.transition            
                        perspective: '800px'
                        rotateY: "#{sign}=270deg"
                        ,750,'easeOutBack', () ->
                            this.transition
                                perspective: '800px'
                                rotateY: "#{antisign}=360deg"
                                ,1,'ease', () ->
                                    self.startTimer()

                    return
        return;

    fillData: (slide) ->
        self = this
        slideBody = $(self.parseTemplate slide).addClass 'iSlide'
        slideBody.css {'width':self.options.width, 'height':self.options.height}
        self.addShadow slideBody,slide.shadow
        self.slidesContainer.append slideBody
        self.totalWidth += $(slideBody).width()
        return;

    rotateContainer: ->

        return;

    destroy: ->
        this.element.next().remove()
        return

    _setOption: (option, value) ->
        $.Widget.prototype._setOption.apply(this, arguments)

        el = this.element
        return
}

$(document).ready ->
    $('.iSlider').iSlider
        templateHtml: """
          <a href="http://{link}" target="_blank" class="slideBody" style="background-image: url('{background}'); border-color:{borderColor}; ">
            <div class="title" style="color:{shadow}">{title}</div>
            <div class="link" style="background-color:{shadow}">{link}</div>
          </a>
        """
        debug: yes
        width: 200
        height: 295
        slides: [
          {
            background:'img/bg_01.png'
            title: 'Халяль<br/>справочник России'
            link: 'www.HALALGID.ru'
            borderColor: '#819F71'
            shadow: '#1C4302'
          },
          {
            background:'img/bg_02.png'
            title: 'Образовательный<br/>портал'
            link: 'www.BAYTALHIKMA.ru'
            borderColor: '#AE8F6F'
            shadow: '#8F3F1D'
          },
          {
            background:'img/bg_03.png'
            title: 'Электронная<br/>онлайн-библиотека'
            link: 'www.DARUL-KUTUB.com'
            borderColor: '#AD8E6F'
            shadow: '#8E3E1B'
          },
          {
            background:'img/bg_04.png'
            title: 'Мусульманский<br/>женский портал'
            link: 'www.ANNISA-TODAY.ru'
            borderColor: '#9BBF9A'
            shadow: '#418845'
          },
          {
            background:'img/bg_05.png'
            title: 'Время намаза'
            link: 'www.NAMAZ-TIME.com'
            borderColor: '#C89182'
            shadow: '#842515'
          }
        ]
    return